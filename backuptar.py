#!/usr/bin/env python3 

import sys 
import os 
import tarfile
import datetime


if os.getuid() != 0:
	sys.stderr.write("ERROR: Must be root to execute \n")
	sys.exit(1)

args = sys.argv[1:]
if not args:
	print('Usage : {} [SOURCE..DIRECTORY] [PUT ARCHIVE TO DIRECTORY]\n' 
		   '\t Exam: /var/spool/cron  /opt' .format(sys.argv[0]))
	sys.exit(0)

def TarFiles(path, ToDirTar):
	if not os.path.exists(path) or not os.path.exists(ToDirTar):
		print("ERROR: The path does not exist \n")
		return -1
	
	t = datetime.datetime.now()
	t_now = t.strftime("%d%m%Y-%I%M")
	FileNameTar = '%s/crontab_%s.tgz' % (ToDirTar, t_now)
	tar = tarfile.open(FileNameTar, 'w:gz')
	
	for f in os.listdir(path):
		if os.path.join(path,f):
			fullpath = os.path.join(path,f)
			tar.add(fullpath)
	tar.close()
	print(FileNameTar)

TarFiles(sys.argv[1], sys.argv[2])
