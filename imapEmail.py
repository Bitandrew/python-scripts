#!/usr/bin/env python3

import os, imaplib, sys

def connect_to_imap(server, username, password, directory='sshd'):
        temp = sys.stdout
        sys.stdout = open('/tmp/imapConnect.log', 'w')

        imapConnect = imaplib.IMAP4_SSL(server)
        conn, imapAccount = imapConnect.login(username, password)
        if conn != 'OK':
                print ('Could not connect to imap server')
                sys.exit(1)

        print ('Connect to imap server successfully')
        print ('Try to select from directory {}'.format(directory))

        conn, data = imapConnect.select(directory)
        print ('Select status connect: {},'.format(conn))
        if conn != 'OK':
                print ('Could select directory {}'.format(directory))
                sys.exit(1)

        conn, mailids = imapConnect.search(None, '(UNSEEN)')
        print (mailids)


        for mailid in mailids[0].split():
                datalist = []
                conn, data = imapConnect.fetch(mailid, '(BODY[HEADER.FIELDS (SUBJECT)])')
                print ('Connect status FETCH : {}'.format(conn))

                if conn != 'OK':
                        print ('Connect status FETCH: {}'.format(conn))


                status = data[0][1][9:]
                if "start" in status.decode('utf8'):
                        print ("Tunel GO")
                        os.system("ssh -Nf -T home1 -R 0:localhost:22")

        sys.stdout.close()
        sys.stdout = temp
        imapConnect.close()



connect_to_imap('outlook.office365.com', 'email', 'somepassword')
