#!/usr/bin/env python3 

import sys 
import os 
import tarfile
import datetime

#if os.getuid() != 0:
#	sys.stderr.write("ERROR: Must be root to execute \n")
#	sys.exit(1)

args = sys.argv[1:]
if not args:
	print('Usage : {} [SOURCE..DIRECTORY] [PUT ARCHIVE TO DIRECTORY]\n' 
		   '\t Exam: /var/spool/cron  /opt' .format(sys.argv[0]))
	sys.exit(0)

t = datetime.datetime.now()
t_now = t.strftime("%d%m%Y-%I%M")
timeago = t-datetime.timedelta(minutes=30)	
FileNameTar = '%s/crontab_%s.tar' % (sys.argv[2], t_now)	

def FilesList(path):
	if not os.path.exists(path):
		print("ERROR: Path does not exist \n")
		return -1

#  Creat Generator with files  
	for f in os.listdir(path):
		if os.path.join(path,f):
			fullpath = os.path.join(path,f)
			StFile = os.path.getmtime(fullpath)
			modtime = datetime.datetime.fromtimestamp(StFile)
			if modtime > timeago:
				yield fullpath


flist = [fl for fl in FilesList(sys.argv[1])]

if len(flist) == 0:
	sys.exit(0)
else:	
	tar = tarfile.open(FileNameTar, 'w')
	for f in flist:		
		tar.add(f)
	tar.close()
print(FileNameTar)


#def CopyToS3(LockFile):
#	if os.path.exists(LockFile):
#		sys.stderr.write("WARNING: aws s3 has been running \n" )
#		return -1
#FilesList(sys.argv[1], sys.argv[2])
	
