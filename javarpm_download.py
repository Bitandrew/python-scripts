#!/usr/bin/env python3

from urllib.request import urlopen
from urllib.error import URLError, HTTPError
from urllib.request import Request
import re,os,sys

'''
Author : Andrew

Given a oracle java url,find the last jre or jdk url and download the rpm.

Here's what a string looks like.

downloads['jre-8u111-oth-JPR']['files']['jre-8u111-linux-x64.rpm'] = { "title":"Linux x64", "size":"52.75 MB","filepath":
"http://download.oracle.com/otn-pub/java/jdk/8u111-b14/jre-8u111-linux-x64.rpm", "MD5":"099044714e758029ca7d875daa54fbfa", 
"SHA256":"97918e36a0542a5256a2047300d89af1b9381a4a2f73de7e34b35bfa6b024267"};
'''
HeadValues = {'Cookie': 'oraclelicense=accept-securebackup-cookie'}	
urldic =  {
		   'jre':'http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html', 
           'jdk': 'http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html'
		  }




def GetUrlRpm(url):
	''' 
	Getting a list of the java urls from the given html.
	extracting the last java rpm url and return it.
	If the url is not correct then the funcntion is returned an error status code.
	'''
	req = Request(url)
	try:
		respon = urlopen(req)
	except HTTPError as Errors:
		print('The server cound not fulfill the request.')
		print('Error code: ', Errors.code)
	except URLError as Errors:
		print("Failed to reach a server.")
		print('Reason:', Errors.code)
	else:
		print('Read data')
		data = respon.read().decode('utf-8')
		rpm_match = re.findall(r'(http:\/\/.*x64\.rpm)', data)
		rpm_url = rpm_match[-1]
		return rpm_url


def DownloadRpm(url, DownloadPath):
	'''
	Getting a rpm url which is obtained from the function GetUtlRpm ,
	extracting java Version, download [jdk,jre]rpm.	
	
	Jfilepath = your/directiory/path
	'''
	req = Request(url, headers=HeadValues)
	try:
		respon = urlopen(req)
	except HTTPError as Errors:
		print('Cound not connect to download {}'.format(url))
		print('Error code:', Errors.code)
	except URLError as Errors:
		print('Failed to reach the {}'.format(url))
	else:
		JavaList = url.split('/')
		JavaVersion = JavaList[-1]
		Jfilepath = DownloadPath + '/' + JavaVersion 
		if os.path.exists(Jfilepath) != True:
			print('Start download rpm {}'.format(JavaVersion))
			JavaRead = respon.read()
			with open(Jfilepath,'wb') as rpm:
				rpm.write(JavaRead)
				print('The package was downloaded')
		else:
			print('The package has already downloaded')


def main():
# Default path to download is '/tmp'
# Change values dest if you want to change dirpath
	dest = '/tmp' 
	if  sys.argv[1:]:
		dest = sys.argv[1]
	print('Download path is %s:'% (dest))
		
	
#Get values from the dictionary urldic 
	for key, values in urldic.items():
		key = GetUrlRpm(values)
		if key and os.path.exists(dest):
			DownloadRpm(key,dest)

main()	
	
	
