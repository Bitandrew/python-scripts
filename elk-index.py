#!/usr/bin/env python3

#!/usr/bin/env python3

import sys, time
import json
from urllib.request import Request
from urllib.request import urlopen
from subprocess import Popen, PIPE




def GetIndex(filename):
	
	dirsnap = {}
	with open(filename, 'r') as f:
		data = json.load(f)
		for line in data['snapshots']:
			snapshot =  line['snapshot'] 
			indices  = line['indices']
			if "mc_app_prod" in '\t'.join(indices):
				dirsnap[snapshot] = indices   
				
					
	return dirsnap

def UploadIndex(snapshotID, indexID):

	headr = {'Content-Type': 'application/json'}

	data = {'indices': None, 'ignore_unavailable': 'true', 'include_global_state': 'false'}
	data['indices'] = indexID
	datas = json.dumps(data).encode('utf8')
	print (datas)
		
	url =  "http://localhost:9200/_snapshot/magento_elk/{}/_restore?pretty".format(snapshotID)
	req = Request(url, data=datas, headers=headr)
	out = urlopen(req)


def GetStat(indexID):

	url = 'http://localhost:9200/_cat/indices/{}/?h=health'.format(indexID)
	req = Request(url)
	out = urlopen(req)
	data = out.read().decode('utf-8')
	rest = data.strip()
	return rest


def IndexDel(indexID, method):
	
	if method == 'POST':
		url = 'http://localhost:9200/{}/_close'.format(indexID)
	else:
		url = 'http://localhost:9200/{}/'.format(indexID)

	cmd = 'curl -X{} {}'.format(method, url)
	proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
	result = proc.wait()
	out, err = proc.communicate()
	if result == 0:
		print("The Index was deleted",  out.decode('utf-8'))
	else:
		print("Could not delete the Index", err.decode('utf-8'))	


def GetData(indexID, name, patter):
	
	url =  "http://localhost:9200/{IDINDEX}/{NAME}/_search/?q={PATTER}&pretty".format(IDINDEX=indexID,NAME=name,PATTER=patter)
	req =  Request(url)
	out =  urlopen(req)
	data = out.read().decode('utf-8')
	data = json.loads(data)
	if data['hits']['total'] == 0:
		print('Nothing in the Index: %s' % (indexID))
	else:
		print ("Searching in Index {}".format(indexID))  
		with open('/tmp/elk.log', 'a+') as filelog:
			filelog.write(indexID + '\n')
			filelog.write(str(data['hits']['hits']) + '\n')
			filelog.write('\n' + '------ New Index ------' + '\n') 




def main():

	dirsnap = GetIndex(fh)
	
	for key, value in dirsnap.items():
		for indx in value:
			snapshot, indexID = key, indx
			print('---',snapshot, indexID,'---')

			UploadIndex(snapshot, indexID)
			st = GetStat(indexID)
	
			while True:
				print('Health status of Index: %s' % (st))
				if st == 'green':
					break
				time.sleep (5)
				st = GetStat(indexID)

			GetData(indexID, "mc-app-prod", "MAG000032178") 
			time.sleep (2)

			GetStat(indexID)

			IndexDel(indexID, method='POST')
			IndexDel(indexID, method='DELETE')
			time.sleep(5)


if __name__ == '__main__':

	if len(sys.argv) > 1:
		fh = sys.argv[1]
	else:
		sys.stderr("Select a json file")
		sys.exit(1)

	main()


		





